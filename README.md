# PRODE Back :soccer:

La app "ProdeQatar" es un sistema de predicciones deportivas, desarrollada especificamente a raiz de la Copa Mundial FIFA Qatar 2022. Esta ideada para un uso reacreativo, para compartir y jugar junto a tus amigos.

## Empezando :point_down:

Estas instrucciones te permitirán obtener una copia del proyecto en tu máquina local.

## Pre-Requisitos :clipboard:

- Navegador Web (Google Chrome, Morzilla Firefox, Chromium, Brave)
- Node Package Manager (NPM)
- Node.JS (19.0.0)
- React.JS (18.2.0)

## Instalación :white_check_mark:

- Para ejecutar este proyecto en tu dispositivo, clone el repositorio y abra la terminal en su editor y ejecute el comando...

```
$ npm start
```

- O puede visitar directamente: https://www.prodeqatar.onrender.com/



## Despliegue :large_orange_diamond:

Nuestro proyecto se encuentra hosteado con Render.com
## Construido con :construction:

- Node.JS (frontend)
- React.JS (backend)
- Firebase (DB)
## Versiones :warning:
- Firebase: 9.10.0
- React.JS: 18.2.0
- Node.JS: 19.0.0
## Autores 

- Calabrese Bruno - brunocala7
- Cirone Francisco - francironee
- Pereyra Tiago - tiagopereyra
- Sabelli Facundo - facusabelli


## Licencia :copyright:

Todos los derechos son reservados para (Prode Qatar)
## Agradecimientos

