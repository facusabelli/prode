const { body , param , query } = require("express-validator");
const { showValidations } = require("../utils/validationUtils");

module.exports.agregarSeleccionValidator = [
    body("nombre").isString().withMessage("El nombre debe estar y tiene que ser una imagen"),
    body("fotoBandera").isString().withMessage("La foto de la bandera tiene que ser un string"),
    body("federacion").isString().withMessage("El nombre de la federacion debe estar y ser un string"),
    showValidations
]

module.exports.modificarSeleccionValidator = [
    body("nombre").isString().optional().withMessage("El nombre debe estar y tiene que ser una imagen"),
    body("fotoBandera").isString().optional().withMessage("La foto de la bandera tiene que ser un string"),
    body("federacion").isString().optional().withMessage("El nombre de la federacion debe estar y ser un string"),
    showValidations
]

