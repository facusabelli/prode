const express = require("express");
const { verificarID, verificarParametros } = require("../utils/validationUtils");
const { agregarSeleccion, modificarSeleccion, borrarSeleccion, verSelecciones, buscarSeleccion } = require("./seleccionController");
const { agregarSeleccionValidator, modificarSeleccionValidator } = require("./seleccionValidator");
const RouterSeleccion = express.Router();

RouterSeleccion.route("/")
    .get(verSelecciones)
    .post(agregarSeleccionValidator, agregarSeleccion)

RouterSeleccion.route("/:id")
    .all(verificarID)
    .get(buscarSeleccion)
    .patch(modificarSeleccionValidator , modificarSeleccion)
    .delete(borrarSeleccion)

module.exports = RouterSeleccion