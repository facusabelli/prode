const { matchedData } = require("express-validator");
const { initializeApp } = require("firebase/app");
const {
  doc,
  setDoc,
  addDoc,
  collection,
  getFirestore,
  updateDoc,
  deleteDoc,
  getDoc,
  getDocs,
  query,
  where,
  startAt,
  limit,
  orderBy,
  startAfter,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const { codigo200, error500, crearDocumento } = require("../utils/utils.js");
const DB = require("../utils/db");
const { response } = require("express");
const db = DB.getInstance().db;

module.exports.agregarSeleccion = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });
  addDoc(collection(db, "Seleccion"), documento)
    .then((docRef) => {
      documento.id = docRef.id;
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.modificarSeleccion = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: false,
  });
  updateDoc(referencia(req.params.id), documento)
    .then((resultado) => {
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.borrarSeleccion = (req, res) => {
  deleteDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, "Seleccion borrada");
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.buscarSeleccion = (req, res) => {
  getDoc(referencia(req.params.id))
    .then((resultado) => {
      const id = resultado.id;
      const valores = resultado.data();

      const documento = crearDocumento(
        id,
        valores.nombre,
        valores.federacion,
        valores.fotoBandera
      )("id", "nombre", "federacion", "fotoBandera");

      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.verSelecciones = (req, res) => {
  let q;
  console.log(req.query.inicio)
  console.log(req.query.direccion)
  
  if(req.query.inicio == "true"){
    q = query(collection(db,"Seleccion"),orderBy("nombre"),startAt(0),limit(8))
  }
  else{
    if(req.query.direccion == "Izquierda"){
      
      q = query(collection(db,"Seleccion"),orderBy("nombre"),startAt(req.query.nombre),limit(8))
    }
    else{
      console.log(req.query.nombre)
      q = query(collection(db,"Seleccion"),orderBy("nombre"),startAfter(req.query.nombre),limit(8))
    }
  }
  
  getDocs(q).then((response) => {
    let datos = [];
    response.forEach((seleccion) => {
      const id = seleccion.id;
      const valores = seleccion.data();

      const documento = crearDocumento(
        id,
        valores.nombre,
        valores.federacion,
        valores.fotoBandera
      )("id", "nombre", "federacion", "fotoBandera");

      datos.push(documento);
    });
    codigo200(res,datos)
  });
};

function referencia(id) {
  return doc(db, "Seleccion", id);
}
