const { initializeApp } = require("firebase/app");
const {
  connectFirestoreEmulator,
  getFirestore,
  getDocs,
  collection,
  deleteDoc,
  doc,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const app = require("../../app");
const { connectAuthEmulator, getAuth } = require("firebase/auth");
const { response } = require("../../app");
const { crearDocumento } = require("../utils/utils.js");
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth(firebaseApp);
const request = require("supertest")(app);

let token = "";
let id = "";

//connectAuthEmulator(auth, "http://localhost:9099");
//connectFirestoreEmulator(db, "localhost", 8080);

const partidos = [
  {
    seleccionLocal: "jfoisnfe",
    seleccionVisitante: "fnosnopfes",
    golesLocal: "Jeoinfoes",
    golesVisitante: "arg",
    fechaDelPartido: "arg",
    ronda: "arg",
    grupo: "arg",
    resultado: "arg",
  },
  {
    seleccionLocal: "jfoisnfe",
    seleccionVisitante: "fnosnopfes",
    golesLocal: "Jeoinfoes",
    golesVisitante: "arg",
    fechaaDelPartido: "arg",
    ronda: "arg",
    grupo: "arg",
    resultado: "arg",
  }
];

beforeAll(async () => {
  const body = {
    email: "testing@gmail.com",
    pass: "111111",
  };
  const register = await request
    .post("/auth/register")
    .set("origin", "http://localhost:3000")
    .send(body);
  const obtenerToken = await request
    .post("/auth/login")
    .set("origin", "http://localhost:3000")
    .send(body);
  token = obtenerToken._body.data;
});

describe("Testing partidos", () => {
  describe("Post Partido", () => {
    test("Tendria que andar bien", async () => {
        const response = await request
            .post("/app/partidos")
            .set("origin", "http://localhost:3000")
            .send(partidos[0]);

        id = response.body.data.id
        

        expect(response.statusCode).toBe(200);
    });
    test("Tendria que andar mal", async () => {
      const response = await request
          .post("/app/partidos")
          .set("origin", "http://localhost:3000")
          .send(partidos[1]);
          
          
      expect(response.statusCode).toBe(401);
  });
  });
  describe("Get Partidos", () => {
    
    test("Tendria que andar bien", async () => {
      const response = await request
        .get("/app/partidos")
        .set("origin", "http://localhost:3000")
      
      expect(response.statusCode).toBe(200)
    });
  });
  describe("Get Partido",() => {
    test("Tendria que andar bien", async () => {
            
      const response = await request.get("/app/partidos/" + id).set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/partidos/gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })
  describe("Borrar Partido",() => {
    test("Tendria que andar bien", async () => {
      const response = await request.delete("/app/partidos/" + id).set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/partidos/gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })

});

afterAll(async () => {
  deleteDoc(referencia(id))
})

function referencia(id){
  return doc(db,"Partido",id)
}