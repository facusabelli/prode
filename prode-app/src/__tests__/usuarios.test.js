const { initializeApp } = require("firebase/app");
const {
  connectFirestoreEmulator,
  getFirestore,
  getDocs,
  collection,
  deleteDoc,
  doc,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const app = require("../../app");
const { connectAuthEmulator, getAuth } = require("firebase/auth");
const { response } = require("../../app");
const { crearDocumento } = require("../utils/utils.js");
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth(firebaseApp);
const request = require("supertest")(app);

let token = "";
let id = "";

//connectAuthEmulator(auth, "http://localhost:9099");
//connectFirestoreEmulator(db, "localhost", 8080);

const usuarios = [
  {
    nombre: "argentina",
    puntosTotales: 0,
    salas: [],
    pronosticos: [],
    pronosticoJugador:{}
  },
  {
    nombre: "argentina",
    puntosTotales: "smfopesmf",
    salas: "fmsmfpesmfe",
    pronosticos: [],
    pronosticoJujador:{}
  }
];

beforeAll(async () => {
  const body = {
    email: "testing@gmail.com",
    pass: "111111",
  };
  const register = await request
    .post("/auth/register")
    .set("origin", "http://localhost:3000")
    .send(body);
  const obtenerToken = await request
    .post("/auth/login")
    .set("origin", "http://localhost:3000")
    .send(body);
  token = obtenerToken._body.data;
});

describe("Testing usuarios", () => {
  describe("Post Usuario", () => {
    test("Tendria que andar bien", async () => {
        const response = await request
            .post("/app/usuarios")
            .set("origin", "http://localhost:3000")
            .send(usuarios[0]);

        id = response.body.data.id
        

        expect(response.statusCode).toBe(200);
    });
    test("Tendria que andar mal", async () => {
      const response = await request
          .post("/app/usuarios")
          .set("origin", "http://localhost:3000")
          .send(usuarios[1]);
          
          
      expect(response.statusCode).toBe(401);
  });
  });
  describe("Get Usuarios", () => {
    
    test("Tendria que andar bien", async () => {
      const response = await request
        .get("/app/usuarios")
        .set("origin", "http://localhost:3000")
    
      
      expect(response.statusCode).toBe(200)
    });
  });
  describe("Get Usuario",() => {
    test("Tendria que andar bien", async () => {
            
      const response = await request.get("/app/usuarios/" + id).set("origin", "http://localhost:3000")


      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/usuarios/gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })
  describe("Post Pronostico Partido",() => {
    const pronosticos = [{
        partido: "mfopsmoef",
        golesApostadosLocal: 1,
        golesApostadosVisitante: 2,
        ganador: "mfeopsmpe"
    },
    {
        partido: "mfopsmoef",
        golesApostadosLocal: 1,
        golesApostadosVisitante: 2,
        ganaddor: "mfeopsmpe"
    }
    ]
    test("Tendria que andar bien", async () => {
      const response = await request.post("/app/usuarios/" + id + "/pronosticos").set("origin", "http://localhost:3000").send(pronosticos[0])

        //console.log(response)
      
      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/usuarios/gkmmepsfse/pronosticos").set("origin", "http://localhost:3000").send(pronosticos[1])

      expect(response.statusCode).toBe(404)
    })
  })

  describe("Post Pronostico Jugador",() => {
    const pronosticos = [{
        mejorJugador: "MFoemsf",
        mejorArquero: "mepofmsfe"
    },
    {
        mejorJugador: "MFoemsf",
        mejorresArquero: "mepofmsfe"
    }
    ]
    test("Tendria que andar bien", async () => {
      const response = await request.post("/app/usuarios/" + id + "/prodeJugador").set("origin", "http://localhost:3000").send(pronosticos[0])

        //console.log(response)
      
      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/usuarios/gkmmepsfse/prodeJugador").set("origin", "http://localhost:3000").send(pronosticos[1])

      expect(response.statusCode).toBe(404)
    })
  })

  describe("Borrar Usuario",() => {
    test("Tendria que andar bien", async () => {
      const response = await request.delete("/app/usuarios/" + id).set("origin", "http://localhost:3000")

      
      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/usuarios/gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })

});

afterAll(async () => {
  deleteDoc(referencia(id))
})

function referencia(id){
  return doc(db,"Seleccion",id)
}