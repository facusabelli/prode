const { initializeApp } = require("firebase/app");
const {
  connectFirestoreEmulator,
  getFirestore,
  getDocs,
  collection,
  deleteDoc,
  doc,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const app = require("../../app");
const { connectAuthEmulator, getAuth } = require("firebase/auth");
const { response } = require("../../app");
const { crearDocumento } = require("../utils/utils.js");
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth(firebaseApp);
const request = require("supertest")(app);

let token = "";
let id = "";

//connectAuthEmulator(auth, "http://localhost:9099");
//connectFirestoreEmulator(db, "localhost", 8080);

const salas = [
    {
        "nombreDeSala":"kfkeposf",
        "usuarioCreador":"flmesopjfs",
        "usuarios": ["fesfes","fsefse"]
    },
    {
        "nombreDeSala":"kfkneposf",
        "usuarioCreador":"flmesopjfs",
        "usuarrios": ["fesfes","fsefse"]
    }
]
    


beforeAll(async () => {
  const body = {
    email: "testing@gmail.com",
    pass: "111111",
  };
  const register = await request
    .post("/auth/register")
    .set("origin", "http://localhost:3000")
    .send(body);
  const obtenerToken = await request
    .post("/auth/login")
    .set("origin", "http://localhost:3000")
    .send(body);
  token = obtenerToken._body.data;
});

describe("Testing Salas", () => {
  describe("Post Sala", () => {
    test("Tendria que andar bien", async () => {
        const response = await request
            .post("/app/salas")
            .set("origin", "http://localhost:3000")
            .send(salas[0]);

        id = response.body.data.id
        

        expect(response.statusCode).toBe(200);
    });
    test("Tendria que andar mal", async () => {
      const response = await request
          .post("/app/salas")
          .set("origin", "http://localhost:3000")
          .send(salas[1]);
          
          
      expect(response.statusCode).toBe(401);
  });
  });
  describe("Get Salas", () => {
    
    test("Tendria que andar bien", async () => {
      const response = await request
        .get("/app/salas")
        .set("origin", "http://localhost:3000")
      
      expect(response.statusCode).toBe(200)
    });
  });
  describe("Get Sala",() => {
    test("Tendria que andar bien", async () => {
      const response = await request.get("/app/salas/" + id).set("origin", "http://localhost:3000")

      

      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/salas/" + "gkmmepsfse").set("origin", "http://localhost:3000")


      expect(response.statusCode).toBe(500)
    })
  })
  describe("Borrar Sala",() => {
    test("Tendria que andar bien", async () => {
      const response = await request.delete("/app/salas/" + id).set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/salas/" + "gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })

});

afterAll(async () => {
  deleteDoc(referencia(id))
})

function referencia(id){
  return doc(db,"Sala",id)
}