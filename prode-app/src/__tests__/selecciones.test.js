const { initializeApp } = require("firebase/app");
const {
  connectFirestoreEmulator,
  getFirestore,
  getDocs,
  collection,
  deleteDoc,
  doc,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const app = require("../../app");
const { connectAuthEmulator, getAuth } = require("firebase/auth");
const { response } = require("../../app");
const { crearDocumento } = require("../utils/utils.js");
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth(firebaseApp);
const request = require("supertest")(app);

let token = "";
let id = "";

//connectAuthEmulator(auth, "http://localhost:9099");
//connectFirestoreEmulator(db, "localhost", 8080);

const selecciones = [
  {
    nombre: "argentina",
    fotoBandera: "smfopesmf",
    federacion: "fmsmfpesmfe"
  },
  {
    nombre: "argentina",
    fotoBandera: "smfopesmf",
    federzacion: "fmsmfpesmfe"
  }
];

beforeAll(async () => {
  const body = {
    email: "testing@gmail.com",
    pass: "111111",
  };
  const register = await request
    .post("/auth/register")
    .set("origin", "http://localhost:3000")
    .send(body);
  const obtenerToken = await request
    .post("/auth/login")
    .set("origin", "http://localhost:3000")
    .send(body);
  token = obtenerToken._body.data;
});

describe("Testing selecciones", () => {
  describe("Post Seleccion", () => {
    test("Tendria que andar bien", async () => {
        const response = await request
            .post("/app/selecciones")
            .set("origin", "http://localhost:3000")
            .send(selecciones[0]);

        id = response.body.data.id
        

        expect(response.statusCode).toBe(200);
    });
    test("Tendria que andar mal", async () => {
      const response = await request
          .post("/app/selecciones")
          .set("origin", "http://localhost:3000")
          .send(selecciones[1]);
          
          
      expect(response.statusCode).toBe(401);
  });
  });
  describe("Get Selecciones", () => {
    
    test("Tendria que andar bien", async () => {
      const response = await request
        .get("/app/selecciones")
        .set("origin", "http://localhost:3000")
      
      expect(response.statusCode).toBe(200)
    });
  });
  describe("Get Seleccion",() => {
    test("Tendria que andar bien", async () => {
            
      const response = await request.get("/app/selecciones/" + id).set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/selecciones/gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })
  describe("Borrar Seleccion",() => {
    test("Tendria que andar bien", async () => {
      const response = await request.delete("/app/selecciones/" + id).set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(200)
    });
    test("Tendria que andar mal", async () => {
      const response = await request.get("/app/selecciones/gkmmepsfse").set("origin", "http://localhost:3000")

      expect(response.statusCode).toBe(500)
    })
  })

});

afterAll(async () => {
  deleteDoc(referencia(id))
})

function referencia(id){
  return doc(db,"Seleccion",id)
}