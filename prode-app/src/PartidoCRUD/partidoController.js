const { matchedData } = require("express-validator");
const { initializeApp } = require("firebase/app");
const {
  addDoc,
  collection,
  updateDoc,
  getDoc,
  getDocs,
  deleteDoc,
  getFirestore,
  doc,
  startAt,
  query,
  orderBy,
  endAt,
  limit,
  startAfter,
  where,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales");

const {
  codigo200,
  transformarReferencia,
  crearDocumento,
  error500,
} = require("../utils/utils");
const DB = require("../utils/db");
const db = DB.getInstance().db;

module.exports.agregarPartido = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  addDoc(collection(db, "Partido"), documento)
    .then((docRef) => {
      documento.id = docRef.id;
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.modificarPartido = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: false,
  });

  updateDoc(referencia(req.params.id), documento)
    .then((resultado) => {
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.modificarPuntos = (req, res) => {};

module.exports.borrarPartido = (req, res) => {
  deleteDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, "Jugador borrado");
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.buscarPartido = (req, res) => {
  getDoc(referencia(req.params.id))
    .then((resultado) => {
      const id = resultado.id;

      const valores = resultado.data();
      const seleccionLocal = valores.seleccionLocal;
      const seleccionVisitante = valores.seleccionVisitante;
      const golesLocal = valores.golesLocal;
      const golesVisitante = valores.golesVisitante;
      const estadio = valores.nombreEstadio;
      const grupo = valores.grupo;
      const ronda = valores.ronda;
      const fechaPartido = valores.fechaPartido;
      const estado = valores.estado;

      const documento = crearDocumento(
        id,
        seleccionLocal,
        seleccionVisitante,
        golesLocal,
        golesVisitante,
        estadio,
        ronda,
        grupo,
        fechaPartido,
        estado
      )(
        "id",
        "seleccionLocal",
        "seleccionVisitante",
        "golesLocal",
        "golesVisitante",
        "estadio",
        "ronda",
        "grupo",
        "fechaPartido",
        "estado"
      );

      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.totalRegistros = (req, res) => {
  getDocs(collection(db, "Partido"))
    .then((response) => {
      codigo200(res, response.size);
    })
    .catch((error) => {
      console.log(error);
    });
};

module.exports.verPartidos = (req, res) => {

  let q = query(
    collection(db, "Partido"),
    where("seleccionLocal", "==", req.query.seleccion)
  );

  getDocs(q)
    .then((resultado) => {
      const datos = [];
      resultado.forEach((doc) => {
        const id = doc.id;

        const valores = doc.data();
        const seleccionLocal = valores.seleccionLocal;
        const seleccionVisitante = valores.seleccionVisitante;
        const golesLocal = valores.golesLocal;
        const golesVisitante = valores.golesVisitante;
        const grupo = valores.grupo;
        const ronda = valores.ronda;
        const fechaPartido = valores.fechaDelPartido;
        const estado = valores.estado;
        const resultadoPartido = valores.resultado;

        const documento = crearDocumento(id,seleccionLocal,seleccionVisitante,golesLocal,golesVisitante,ronda,grupo,fechaPartido,estado,resultadoPartido)("id","seleccionLocal","seleccionVisitante","golesLocal","golesVisitante","ronda","grupo","fechaPartido","estado","resultado");
        datos.push(documento);
      });
      let q = query(
        collection(db, "Partido"),
        where("seleccionVisitante", "==", req.query.seleccion)
      );
      getDocs(q)
      .then((resultado2) => {
        resultado2.forEach((doc) => {
          const id = doc.id;
  
          const valores = doc.data();
          const seleccionLocal = valores.seleccionLocal;
          const seleccionVisitante = valores.seleccionVisitante;
          const golesLocal = valores.golesLocal;
          const golesVisitante = valores.golesVisitante;
          const grupo = valores.grupo;
          const ronda = valores.ronda;
          const fechaPartido = valores.fechaDelPartido;
          const estado = valores.estado;
          const resultadoPartido = valores.resultado;
  
          const documento = crearDocumento(id,seleccionLocal,seleccionVisitante,golesLocal,golesVisitante,ronda,grupo,fechaPartido,estado,resultadoPartido)("id","seleccionLocal","seleccionVisitante","golesLocal","golesVisitante","ronda","grupo","fechaPartido","estado","resultado");
          if(req.query.todo == "1" || estado == "no empezado"){
            datos.push(documento);
          }
        });
        codigo200(res, datos);
      })
    })
    .catch((error) => {
      error500(res, error);
    });
};

function referencia(id) {
  return doc(db, "Partido", id);
}
