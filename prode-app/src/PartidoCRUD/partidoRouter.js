const express = require("express");
const { actualizarPuntos } = require("../usuariosCRUD/usuarioController");
const { verificarID, verificarParametros } = require("../utils/validationUtils");
const { agregarPartido, modificarPartido, verPartidos, buscarPartido, borrarPartido, totalRegistros } = require("./partidoController");
const { agregarPartidoValidator, modicicarPartidoValidator } = require("./partidoValidator");
const RouterPartido = express.Router();

RouterPartido.route("/")
    .get(verPartidos)
    .post(agregarPartidoValidator,agregarPartido)

RouterPartido.route("/total")
    .get(totalRegistros)

RouterPartido.route("/:id")
    .all(verificarID)
    .get(buscarPartido)
    .patch(modicicarPartidoValidator, actualizarPuntos ,modificarPartido)
    .delete(borrarPartido)

module.exports = RouterPartido