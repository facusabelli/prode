const { body } = require("express-validator");
const { showValidations } = require("../utils/validationUtils");

module.exports.agregarPartidoValidator = [
    body("seleccionLocal").isString().withMessage("El nombre de la selección local tiene que estar"),
    body("seleccionVisitante").isString().withMessage("El nombre de la seleccion visitante tiene que estar "),
    body("golesLocal").isString().withMessage("Los goles de la seleccion local tiene que estar "),
    body("golesVisitante").isString().withMessage("Los goles de la seleccion visitante tiene que estar "),
    body("fechaDelPartido").isString().withMessage("La fecha del partido tiene que estar"),
    body("ronda").isString().withMessage("El nombre de la ronda tiene que estar"),
    body("grupo").isString().optional().withMessage("El grupo tiene que ser un string"),
    body("resultado").isString().withMessage("El resultado tiene que estar"),
    body("estado").isString().withMessage("El estado tiene que estar"),
    showValidations
]

module.exports.modicicarPartidoValidator = [
    body("seleccionLocal").isString().optional().withMessage("El nombre de la selección local tiene que estar"),
    body("seleccionVisitante").isString().optional().withMessage("El nombre de la seleccion visitante tiene que estar "),
    body("golesLocal").isString().optional().withMessage("Los goles de la seleccion local tiene que estar "),
    body("golesVisitante").isString().optional().withMessage("Los goles de la seleccion visitante tiene que estar "),
    body("fechaDelPartido").isString().optional().withMessage("La fecha del partido tiene que estar"),
    body("ronda").isString().optional().withMessage("El nombre de la ronda tiene que estar"),
    body("grupo").isString().optional().withMessage("El grupo tiene que ser un string"),
    body("resultado").isString().optional().withMessage("El resultado tiene que estar"),
    body("estado").isString().optional().withMessage("El estado tiene que estar"),
    showValidations
]
