const { initializeApp } = require("firebase/app")
const { firebaseConfig } = require("../Firebase/credenciales.js")
const { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, GoogleAuthProvider, signInWithPopup , sendEmailVerification, signOut } = require("firebase/auth");
const { codigo200, error401, error500 } = require("../utils/utils.js");
const { response } = require("express");
const { getDoc, getFirestore, doc } = require("firebase/firestore");

const DB=require("../utils/db");
const db = DB.getInstance().db

const auth = getAuth();
const provider = new GoogleAuthProvider();



module.exports.register = (req, res, next) => {
  createUserWithEmailAndPassword(auth, req.body.email, req.body.pass)
    .then((response) => {
      req.userId = response._tokenResponse.localId
      next()
    })
    .catch((error) => {
      error401(res,error.code)
    })
}

module.exports.login = (req, res, next) => {
  signInWithEmailAndPassword(auth, req.body.email, req.body.pass)
    .then((response) => {
      req.userId = response._tokenResponse.localId
      console.log(req.userId)
      getDoc(referencia(req.userId))
      .then(response => {
        
        req.currentUser = response.data()
        if(auth.currentUser.emailVerified){
          next()
        }
        else{
          
          error401(res,"Email no verificado")
        }
      })
      
      
    })
    .catch((error) => {
      console.log(error)
      error401(res,error)
    })
}

module.exports.logout = (req, res, next) => {

  

  signOut(auth)
  .then(resultado => {
    console.log("llegue aca")
    res.clearCookie("nombre")
    res.clearCookie("puntos")
    res.clearCookie("token")
    res.clearCookie("isLogged")
    res.clearCookie("admin")
    res.clearCookie("id")
    codigo200(res,resultado);
  })
  .catch(error => {
    error500(res,error)
  })
}

module.exports.sendLinkVerification = (req, res, next) => {
  const user = auth.currentUser

  sendEmailVerification(user)
  .then(response => {
    next()
  })
}

function referencia(id) {
  return doc(db, "Usuario", id)
}