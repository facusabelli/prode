const config = require("config");
const jwt = require("jsonwebtoken");
const { error403, error401 } = require("./utils");

module.exports.createToken = (body) => {
    return new Promise((resolve,reject) => {
        
        jwt.sign(body, config.get("authentication.jwtSecret"), {expiresIn: config.get("authentication.jwtExpiry")},(error,token) => {
            if(error) reject(error)

            resolve(token)
        })
    })
}

module.exports.readToken = (req,res,next) => {
    const authorizationToken = req.header.authorization ? req.header.authorization.split(':').split(' ')[1] : undefined
    const cookieToken = req.cookies.token 

    const token = authorizationToken || cookieToken 
    
    if(!token)
        error401(res,"No estas logueado")
    
    verifyToken(token)
    .then((tokenData) => {
        req.userData = {
            id: tokenData.id,
            email: tokenData.email,
            username: tokenData.username
        }
        console.log(tokenData)
        next()
    })
    .catch((error) => {
        error401(res,"No estas logueado")
    })
    
}

const verifyToken = (token) => {
    return new Promise((resolve,reject) => {
        jwt.verify(token,config.get("authentication.jwtSecret"), (error,tokenData) => {
            if(error) return reject(error)
            
            resolve(tokenData)
        })
    })
} 
