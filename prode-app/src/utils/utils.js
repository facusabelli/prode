
module.exports.codigo200 = (res,mensaje) => {
    return res.status(200).json({
        code: 10,
        data: mensaje
    })
}

module.exports.error400 = (res,mensaje) => {
    return res.status(401).json({
        code: 20,
        data: mensaje
    })
}

module.exports.error401 = (res,mensaje) => {
    return res.status(401).json({
        code: 21,
        data: mensaje
    })
}

module.exports.error403 = (res,mensaje) => {
    return res.status(403).json({
        code: 23,
        data: mensaje
    })
}

module.exports.error404 = (res,mensaje) => {
    return res.status(404).json({
        code: 24,
        data: mensaje
    })
}

module.exports.error500 = (res,mensaje) => {
    return res.status(500).json({
        code: 30,
        data: mensaje
    })
}

module.exports.crearDocumento = (...valores) => {
    return function(...campos) {
        const objeto = {}
        campos.forEach((campo,i) => {
            objeto[campo] = valores[i]
        })
        return objeto
    }
}

module.exports.transformarReferencia = (documentRef) => {
    return documentRef.segments[documentRef.segments.length - 1]
}