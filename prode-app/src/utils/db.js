const { initializeApp } = require("firebase/app");
const { firebaseConfig } = require("../Firebase/credenciales.js")
const {
  addDoc,
  getFirestore,
  collection,
  updateDoc,
  getDoc,
  getDocs,
  doc,
  deleteDoc,
  setDoc,
  increment,
  arrayRemove,
} = require("firebase/firestore");

class DBSingleton {
    constructor() {
        this.firebaseApp = initializeApp(firebaseConfig);
        this.db = getFirestore(this.firebaseApp);

    }
}

class DB {
    constructor() {
        throw new Error('Use Singleton.getInstance()');
    }
    static getInstance() {
        if (!DB.instance) {
            DB.instance = new DBSingleton();
        }
        return DB.instance;
    }
}
module.exports = DB;