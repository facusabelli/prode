const { validationResult , param, body } = require('express-validator');
const { error400 } = require("./utils")

module.exports.showValidations = (req,res,next) => {

    const errors = validationResult(req)

    if (errors.isEmpty())
        return next()

    error400(res,errors.array())
}

module.exports.verificarID = [
    param("id").isAlphanumeric().withMessage("La id debe estar como parametro y debe ser alfanumérica"),
    this.showValidations
]

module.exports.verificarParametros = [
    param("inicio").isNumeric().withMessage("El inicio tiene que estar y debe ser numérico"),
    param("limit").isNumeric().withMessage("El límite tiene que estar y debe ser numérico"),
    this.showValidations
]