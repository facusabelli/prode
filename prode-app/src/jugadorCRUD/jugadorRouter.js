const express = require("express");
const { verificarID, verificarParametros } = require("../utils/validationUtils");
const { agregarJugador, modificarJugador, borrarJugador, verSelecciones,  buscarJugador, verJugadores } = require("./jugadorController");
const { agregarJugadorValidator, modificarJugadorValidator } = require("./jugadorValidator");
const RouterJugador = express.Router();

RouterJugador.route("/")
    .post(agregarJugadorValidator,agregarJugador)
    .get(verJugadores)

RouterJugador.route("/:id")
    .all(verificarID)
    .get(buscarJugador)
    .patch(modificarJugadorValidator,modificarJugador)
    .delete(borrarJugador)


module.exports = RouterJugador