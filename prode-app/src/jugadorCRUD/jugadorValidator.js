const { body } = require("express-validator");
const { showValidations } = require("../utils/validationUtils");




module.exports.agregarJugadorValidator = [
    body("nombreJugador").isString().withMessage("El nombre del jugador tiene que estar"),
    body("posicion").isString().withMessage("La posicion del jugador tiene que estar"),
    body("foto").isString().withMessage("La foto del jugador tiene que estar"),
    body("seleccion").isString().withMessage("La seleccion del jugador tiene que estar"),
    showValidations
]

module.exports.modificarJugadorValidator = [
    body("nombreJugador").isString().optional().withMessage("El nombre del jugador tiene que ser un string"),
    body("posicion").isString().optional().withMessage("La posicion del jugador tiene que un string"),
    body("foto").isString().optional().withMessage("La foto del jugador tiene que ser un string"),
    body("seleccion").isString().optional().withMessage("La seleccion del jugador tiene que ser un string"),
    showValidations
]