const { matchedData } = require("express-validator");
const { initializeApp } = require("firebase/app");
const {
  doc,
  setDoc,
  addDoc,
  collection,
  getFirestore,
  updateDoc,
  deleteDoc,
  query,
  where,
  getDocs,
  getDoc,
  orderBy,
  startAfter,
  startAt,
  limit,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const {
  codigo200,
  error500,
  crearDocumento,
  transformarReferencia,
} = require("../utils/utils.js");
const DB=require("../utils/db");
const db = DB.getInstance().db

module.exports.agregarJugador = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  addDoc(collection(db, "Jugador"), documento)
    .then((docRef) => {
      documento.id = docRef.id;
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.modificarJugador = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: false,
  });

  updateDoc(referencia(req.params.id), documento)
    .then((resultado) => {
      console.log(resultado);
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.borrarJugador = (req, res) => {
  deleteDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, "Jugador borrado");
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.buscarJugador = (req, res) => {
  getDoc(referencia(req.params.id))
    .then((resultado) => {
      const id = resultado.id;
      const valores = resultado.data();

      const documento = crearDocumento(
        id,
        valores.nombreJugador,
        valores.posicion,
        valores.fotoBandera,
        valores.seleccion
      )("id", "nombreJugador", "posicion", "foto", "selecion");

      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.verJugadores = (req,res) => {
  //let q = query(collection(db, "Jugador"),orderBy("nombreJugador"),startAt(req.query.inicio),limit(req.query.limit))
  let q = query(collection(db,"Jugador"),where("seleccion","==",req.query.seleccion))
  getDocs(q)
    .then((resultado) => {
      const datos = [];
      resultado.forEach((doc) => {
        const id = doc.id;
        const valores = doc.data();

        const documento = crearDocumento(
          id,
          valores.nombreJugador,
          valores.posicion,
          valores.foto,
          valores.seleccion
        )("id", "nombreJugador", "posicion", "foto", "selecion");

        datos.push(documento);
      });
      codigo200(res, datos);
    })
    .catch((error) => {
      error500(res, error);
    });
}

function referencia(id) {
  return doc(db, "Jugador", id);
}
