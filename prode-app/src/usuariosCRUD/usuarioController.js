const { response } = require("express");
const { matchedData } = require("express-validator");
const { initializeApp } = require("firebase/app");
const DB = require("../utils/db");
const {
  addDoc,
  getFirestore,
  collection,
  updateDoc,
  getDoc,
  getDocs,
  doc,
  deleteDoc,
  setDoc,
  increment,
  arrayRemove,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales");
const {
  codigo200,
  error400,
  error500,
  crearDocumento,
} = require("../utils/utils");

const db = DB.getInstance().db;

module.exports.agregarUsuario = (req, res, next) => {
  const documento = {
    nombre: req.body.user,
    puntosTotales: 0,
    salas: [],
    pronosticos: [],
    pronosticoJugador: {},
  };

  setDoc(referencia(req.userId), documento)
    .then((resultado) => {
      documento.id = resultado.id;
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.agregarPronostico = (req, res, next) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  let array = [];

  getDoc(referencia(req.params.id)).then((response) => {
    data = response.data();

    array = data.pronosticos;

    // if(array.includes(documento)){
    //   console.log("Esta Incluido")
    // }
    // else{
    //   console.log("No Incluido")
    // }

    if (array.filter((p) => p.partido == documento.partido).length > 0) {
      let pronosticosModificados = array;

      pronosticosModificados.forEach((pronostico) => {
        if (pronostico.partido == documento.partido) {
          const fields1 = Object.keys(pronostico);
          const fields2 = Object.keys(documento);

          fields1.forEach((field) => {
            fields2.forEach((field2) => {
              if (field == field2) {
                pronostico[field] = documento[field2];
              }
            });
          });
        }
      });

      updateDoc(referencia(req.params.id), {
        pronosticos: pronosticosModificados,
      }).then((response) => {
        codigo200(res, response);
      });
    } else {
      array.push(documento);
      updateDoc(referencia(req.params.id), {
        pronosticos: array,
      })
        .then((resultado) => {
          codigo200(res, documento);
        })
        .catch((error) => {
          error500(res, error);
        });
    }
  });
};

module.exports.modificarPronostico = (req, res, next) => {
  const body2 = matchedData(req, {
    locations: ["body"],
    includeOptionals: false,
  });

  getDoc(referencia(req.params.id))
    .then((resultado) => {
      const valores = resultado.data();

      const documento = crearDocumento(
        resultado.id,
        valores.nombre,
        valores.puntosTotales,
        valores.pronosticos,
        valores.pronosticoJugador,
        valores.salas
      )(
        "id",
        "nombre",
        "puntosTotales",
        "pronosticos",
        "pronosticoJugador",
        "salas"
      );

      let pronosticosModificados = documento.pronosticos;

      pronosticosModificados.forEach((pronostico) => {
        if (pronostico.partido == body2.partido) {
          const fields1 = Object.keys(pronostico);
          const fields2 = Object.keys(body2);

          fields1.forEach((field) => {
            fields2.forEach((field2) => {
              if (field == field2) {
                pronostico[field] = body2[field2];
              }
            });
          });
        }
      });

      updateDoc(referencia(req.params.id), {
        pronosticos: pronosticosModificados,
      }).then((response) => {
        codigo200(res, response);
      });
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.agregarSala = (req, res, next) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  let array = [];

  getDoc(referencia(req.params.id)).then((response) => {
    data = response.data();

    array = data.salas;
    array.push(documento);
    updateDoc(referencia(req.params.id), {
      salas: array,
    })
      .then((resultado) => {
        codigo200(res, documento);
      })
      .catch((error) => {
        error500(res, error);
      });
  });
};

module.exports.verUsuarios = (req, res) => {
  getDocs(collection(db, "Usuario"))
    .then((resultado) => {
      const datos = [];
      resultado.forEach((doc) => {
        const id = doc.id;
        const valores = doc.data();

        const documento = crearDocumento(
          id,
          valores.nombre,
          valores.puntosTotales,
          valores.pronosticos,
          valores.pronosticoJugador,
          valores.salas
        )(
          "id",
          "nombre",
          "puntosTotales",
          "pronosticos",
          "pronosticoJugador",
          "salas"
        );
        datos.push(documento);
      });
      codigo200(res, datos);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.modificarUsuario = (req, res, next) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: false,
  });
  updateDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.agregarPronosticoJugador = (req, res, next) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  updateDoc(referencia(req.params.id), {
    pronosticoJugador: documento,
  })
    .then((resultado) => {
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.borrarUsuario = (req, res, next) => {
  deleteDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, "Usuario borrado");
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.buscarUsuario = (req, res, next) => {
  getDoc(referencia(req.params.id))
    .then((resultado) => {
      const valores = resultado.data();

      const documento = crearDocumento(
        resultado.id,
        valores.nombre,
        valores.puntosTotales,
        valores.pronosticos,
        valores.pronosticoJugador,
        valores.salas
      )(
        "id",
        "nombre",
        "puntosTotales",
        "pronosticos",
        "pronosticoJugador",
        "salas"
      );
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.actualizarPuntos = (req, res, next) => {
  console.log(req.params.id)
  getDoc(doc(db, "Partido", req.params.id)).then((partidoData) => {
    if (req.body.estado == "finalizado" || req.body.resultado != undefined) {
      partido = partidoData.data();
      getDocs(collection(db, "Usuario"))
        .then((resultado) => {
          const usuarios = [];
          resultado.forEach((doc) => {
            const id = doc.id;
            const valores = doc.data();

            const documento = crearDocumento(
              id,
              valores.nombre,
              valores.puntosTotales,
              valores.pronosticos,
              valores.pronosticoJugador,
              valores.salas
            )(
              "id",
              "nombre",
              "puntosTotales",
              "pronosticos",
              "pronosticoJugador",
              "salas"
            );
            usuarios.push(documento);
          });
          usuarios.forEach((usuario) => {
            const pronosticos = usuario.pronosticos;
            const newUser = usuario;

            let puntos = 0;

            pronosticos.forEach((pronostico) => {
              if (pronostico.partido == req.params.id) {
                if (
                  pronostico.golesApostadosLocal == partido.golesLocal &&
                  pronostico.golesApostadosVisitante == partido.golesVisitante
                ) {
                  puntos = 3;
                } else if (pronostico.ganador == partido.resultado) {
                  puntos = 1;
                }
                updateDoc(referencia(newUser.id), {
                  puntosTotales: increment(puntos),
                }).then((res) => {
                  next();
                });
              }
            });
          });
          next();
        })
        .catch((error) => {
          error500(res, error);
        });
    }
  });

  next();
};

function referencia(id) {
  return doc(db, "Usuario", id);
}
