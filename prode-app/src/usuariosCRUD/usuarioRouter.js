const express = require("express");
const { verificarID } = require("../utils/validationUtils");
const {agregarUsuario,agregarPronostico,modificarPronostico,buscarUsuario, agregarPronosticoJugador, borrarUsuario, verUsuarios, agregarSala,} = require("./usuarioController");
const {agregarUsuarioValidator,agregarPronosticoValidator,modificarPronosticoValidator, agregarPronosticoJugadorValidator, agregarSalaValidator,} = require("./usuarioValidator");
const RouterUsuario = express.Router();

RouterUsuario.route("/")
    .post(agregarUsuarioValidator, agregarUsuario)
    .get(verUsuarios)
RouterUsuario.route("/:id")
    .get(verificarID,buscarUsuario)
    .delete(borrarUsuario)
RouterUsuario.route("/:id/salas")
    .post(agregarSalaValidator,agregarSala)
RouterUsuario.route("/:id/pronosticos")
    .post(agregarPronosticoValidator,agregarPronostico)
    .patch(modificarPronosticoValidator,modificarPronostico)
RouterUsuario.route("/:id/prodejugador")
    .post(agregarPronosticoJugadorValidator,agregarPronosticoJugador)
    
module.exports = RouterUsuario;