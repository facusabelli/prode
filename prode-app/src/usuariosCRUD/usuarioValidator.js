const { body } = require("express-validator");
const { showValidations } = require("../utils/validationUtils");

module.exports.agregarUsuarioValidator = [
    body("nombre").isString().withMessage("El nombre debe ser un string"),
    body("puntosTotales").isNumeric().isIn([0]).withMessage("Los puntos deben ser un número igual a 0"),
    body("salas").isArray().withMessage("Las salas deben ser un Array"),
    body("pronosticos").isArray().withMessage("Los pronosticos deben ser un Array"),
    body("pronosticoJugador").isObject().withMessage("El pronostico de jugadores debe ser un objeto"),
    showValidations
]

module.exports.agregarPronosticoValidator = [
    body("partido").isAlphanumeric().withMessage("La referencia al partido tiene que ser alfanumérica"),
    body("golesApostadosLocal").isNumeric().withMessage("Los goles del local tienen que ser numéricos"),
    body("golesApostadosVisitante").isNumeric().withMessage("Los goles del visitante tienen que ser numéricos"),
    body("ganador").isString().withMessage("El ganador tiene que ser un string"),
    showValidations
]

module.exports.modificarPronosticoValidator = [
    body("partido").isString().optional().withMessage("La referencia al partido tiene que ser alfanumérica"),
    body("golesApostadosLocal").isNumeric().optional().withMessage("Los goles del local tienen que ser numéricos"),
    body("golesApostadosVisitante").isNumeric().optional().withMessage("Los goles del visitante tienen que ser numéricos"),
    body("ganador").isString().optional().withMessage("El ganador tiene que ser un string"),
    showValidations
]

module.exports.agregarPronosticoJugadorValidator = [
    body("mejorJugador").isString().withMessage("El nombre del jugador apostado tiene que estar y debe ser un string"),
    showValidations
]

module.exports.agregarSalaValidator = [
    body("idSala").isString().withMessage("La id de la sala tiene que estar"),
    body("nombreSala").isString().withMessage("El nombre de la sala tiene que estar"),
    showValidations
]