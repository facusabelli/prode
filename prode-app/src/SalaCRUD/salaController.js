const { matchedData } = require("express-validator");
const { initializeApp } = require("firebase/app");
const {
  doc,
  setDoc,
  addDoc,
  collection,
  getFirestore,
  updateDoc,
  deleteDoc,
  getDoc,
  getDocs,
  DocumentReference,
} = require("firebase/firestore");
const { firebaseConfig } = require("../Firebase/credenciales.js");
const {
  codigo200,
  error500,
  transformarReferencia,
  crearDocumento,
} = require("../utils/utils.js");
const DB=require("../utils/db");
const db = DB.getInstance().db

//cambiar en firebase el tipo de datos de usuarios
module.exports.agregarSala = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  
  addDoc(collection(db, "Sala"), documento)
    .then((docRef) => {
      documento.id = docRef.id;
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.modificarSala = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: false,
  });
  updateDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.borrarSala = (req, res) => {
  deleteDoc(referencia(req.params.id), req.body)
    .then((resultado) => {
      codigo200(res, "Sala borrada");
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.buscarSala = (req, res) => {
  getDoc(referencia(req.params.id))
    .then((resultado) => {
      const id = resultado.id;
      const valores = resultado.data();
      const documento = crearDocumento(
        id,
        valores.nombre,
        valores.usuarioCreador,
        valores.usuarios,
        valores.contraseñadoc
      )("id", "nombre", "usuarioCreador", "usuarios", "contraseña");

      codigo200(res, documento);
    })
    .catch((error) => {
      error500(res, error);
    });
};

module.exports.verSalas = (req, res) => {
  getDocs(collection(db, "Sala"))
    .then((resultado) => {
      const datos = [];
      resultado.forEach((doc) => {
        const id = doc.id;

        const valores = doc.data();

        

        const documento = {
          id,
          nombre: valores.nombreDeSala,
          contraseña: valores.contraseña,
          usuarioCreador: valores.usuarioCreador,
          usuarios: valores.usuarios,
        };

        datos.push(documento);
      });
      codigo200(res, datos);
    })
    .catch((err) => {
      error500(res, err);
    });
};

module.exports.agregarUsuario = (req, res) => {
  const documento = matchedData(req, {
    locations: ["body"],
    includeOptionals: true,
  });

  let array = [];

  getDoc(referencia(req.params.id)).then((response) => {
    data = response.data();

    console.log(data);

    array = data.usuarios;
    array.push(documento);
    updateDoc(referencia(req.params.id), {
      usuarios: array,
    })
      .then((resultado) => {
        codigo200(res, documento);
      })
      .catch((error) => {
        console.log(error);
        error500(res, error);
      });
  });
};

function referencia(id) {
  return doc(db, "Sala", id);
}
