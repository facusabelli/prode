const { body , param , query } = require("express-validator");
const { showValidations } = require("../utils/validationUtils");

module.exports.agregarSalaValidator = [
    body("nombreDeSala").isString().withMessage("El nombre debe estar y tiene que ser una imagen"),
    body("usuarioCreador").isAlphanumeric().withMessage("La id del usuario creador debe ser alfanumérico"),
    body("usuarios").isArray().withMessage("Los usuarios deben estar incluidos en un array"),
    body("contraseña").isString().withMessage("La contraseña tiene que estar"),
    showValidations
]

module.exports.modificarSalaValidator = [
    body("nombreDeSala").isString().optional().withMessage("El nombre debe estar y tiene que ser una imagen"),
    body("usuarioCreador").isAlphanumeric().optional().withMessage("La id del usuario creador debe ser alfanumérico"),
    body("usuarios").isArray().optional().withMessage("Los usuarios deben estar incluidos en un array"),
    body("contraseña").isString().optional().withMessage("La contraseña tiene que estar"),
    showValidations
]

module.exports.agregarUsuarioValidator = [
    body("idUsuario").isString().withMessage("La id del usuario tiene que estar"),
    body("nombreUsuario").isString().withMessage("El nombre del usuario tiene que estar"),
    body("puntosTotales").isNumeric().withMessage("Los puntosTotales tiene que estar"),
    showValidations
]
