const express = require("express");
const { verificarID } = require("../utils/validationUtils");
const { verSalas, agregarSala, buscarSala, borrarSala, modificarSala, agregarUsuario } = require("./salaController");
const { agregarSalaValidator, modificarSalaValidator, agregarUsuarioValidator } = require("./salaValidator");
const RouterSala = express.Router();

//http://localhost:3001/app/salas
RouterSala.route("/")
    .get(verSalas)
    .post(agregarSalaValidator, agregarSala)

//http://localhost:3001/app/salas/giun48539jih453i54o3
RouterSala.route("/:id")
    .all(verificarID)
    .get(buscarSala)
    .patch(modificarSalaValidator, modificarSala)
    .delete(borrarSala)

//http://localhost:3001/app/salas/giun48539jih453i54o3/usuario
RouterSala.route("/:id/usuario")
    .post(agregarUsuarioValidator,agregarUsuario)

module.exports = RouterSala