const { cookie, body } = require("express-validator");
const { showValidations } = require("../utils/validationUtils");

module.exports.loginValidator = [
    body("email").isString().withMessage("Tiene que estar el mail"),
    body("pass").isAlphanumeric().withMessage("Tiene que estar la contraseña"),
    showValidations
]

module.exports.jwtValidator = [
    cookie("token")
    .isJWT()
    .withMessage("Debe ser JWT"),
    showValidations
]