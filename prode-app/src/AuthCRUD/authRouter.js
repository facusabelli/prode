const express = require("express");
const AuthRouter = express.Router();
const { login, register, sendLinkVerification, logout } = require("../Firebase/firebaseAuth");
const { agregarUsuario } = require("../usuariosCRUD/usuarioController");
const { createSessionToken, sendLoginResponse } = require("./authController");
const { loginValidator } = require("./authValidator");

AuthRouter.post('/register', register, sendLinkVerification, agregarUsuario)
AuthRouter.post('/login', loginValidator, login, createSessionToken, sendLoginResponse)
AuthRouter.post('/logout',logout)

module.exports = AuthRouter;