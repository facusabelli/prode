const { createToken } = require("../utils/tokenUtils");
const { codigo200 } = require("../utils/utils");

module.exports.createSessionToken = (req, res, next) => {
    const userSession = {
        "id": req.userId,
        "email": req.body.email,
    }

    createToken(userSession) 
    .then(token => {
        res.userToken = token
        next()
    })
    .catch(error => {
        res.send(error)
    })
}

module.exports.sendLoginResponse = (req,res) => {
    const token = res.userToken

    const options = {
        httpOnly: false,
        sameSite: 'none',
        secure: true,
        maxAge: 3600 * 1000,
        // domain: "prodeqatar.onrender.com"
    }

    admin = req.body.email == "prodeqatarpoli@gmail.com" ? true : false

    res.cookie("nombre",req.currentUser.nombre,{...options})
    res.cookie("puntos",req.currentUser.puntosTotales,{...options})
    res.cookie("token",token,{...options})
    res.cookie("isLogged",true,{...options})
    res.cookie("admin",admin,{...options})
    res.cookie("id",req.userId,{...options})

    const respuesta = {
        "nombre":req.currentUser.nombre,
        "puntos":req.currentUser.puntosTotales,
        "token":token,
        "isLogged":true,
        "admin":admin,
        "id":req.userId
    }

    codigo200(res,respuesta)
}