const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors')

const app = express();

const authRouter = require('./src/AuthCRUD/authRouter')
const seleccionRouter = require("./src/SeleccionesCRUD/seleccionRouter");
const jugadorRouter = require('./src/jugadorCRUD/jugadorRouter');
const { readToken } = require('./src/utils/tokenUtils');
const { jwtValidator } = require('./src/AuthCRUD/authValidator');
const { Router } = require('express');
const RouterApp = require('./RouterApp');
const WebSocket = require("ws");

// const wss = new WebSocket.Server({
//     port: 7070
// })
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));




// wss.on("connection",(ws) => {
//     console.log(ws.toString() + " conectado")

//     ws.on("message", (mensajeString) => {

//         console.log(mensajeString.toString())
        
//         wss.clients.forEach(client => {
//             if(client != ws){
//                 client.send(mensajeString.toString())
//             } 
//         })
        
//     })

//     ws.on("close",client => {
//         console.log(client.toString() + " desconectado!")
//     })

//     const ping = function() {
//         ws.ping(noop);
//     }

//     setInterval(ping,200000)
// })


app.use(cors({
    credentials: true, origin: function (origin, callback) {
        if (origin == "http://localhost:3000" || origin == "https://prodeqatar.onrender.com") {
            callback(null, true)
        }
        else {
            callback(new Error("Not allowed by CORS"))
        }
    }
}))


app.use("/auth", authRouter);
app.use("/app", /*jwtValidator, readToken,*/ RouterApp)

module.exports = app;
