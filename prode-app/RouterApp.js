const express = require("express");
const { loginValidator } = require("./src/AuthCRUD/authValidator");
const RouterJugador = require("./src/jugadorCRUD/jugadorRouter");
const RouterPartido = require("./src/PartidoCRUD/partidoRouter");
const RouterSala = require("./src/SalaCRUD/salaRouter");
const RouterSeleccion = require("./src/SeleccionesCRUD/seleccionRouter");
const RouterUsuario = require("./src/usuariosCRUD/usuarioRouter");
const RouterApp = express.Router();

RouterApp.use("/selecciones",  RouterSeleccion)
RouterApp.use("/jugadores",  RouterJugador)
RouterApp.use("/salas",RouterSala)
RouterApp.use("/usuarios", RouterUsuario)
RouterApp.use("/partidos",  RouterPartido)

module.exports = RouterApp;